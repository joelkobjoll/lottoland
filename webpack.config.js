// Minifiers
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

// Global
var production = process.env.NODE_ENV === 'production';
var webpack = require('webpack');
var path = require('path');

function get_plugins() {
	var plugins = [];

	plugins.push(new ExtractTextPlugin('css/[name].css'));

	plugins.push(new webpack.DefinePlugin({
		'process.env': { 'NODE_ENV': process.env.NODE_ENV }
	}));

	if (production) {       
		plugins.push(new webpack.optimize.UglifyJsPlugin({
			output: { comments: false },
			sourceMap: false,
			compress: { warnings: false }
		}));
  
		plugins.push(new OptimizeCssAssetsPlugin());
	}

	return plugins;
}

var ProjectPath = path.join(__dirname, 'media');

module.exports = {  
	entry: {
		'script': path.join(ProjectPath, 'ts/main.ts'),
		'style': path.join(ProjectPath, 'scss/_lottoland.scss')
	},
	output: {
		filename: 'js/[name].js',
		path: ProjectPath,
		publicPath: ProjectPath
	},    
	resolve: {
		alias: {
			// Common
			ProjectPath: path.join(ProjectPath, 'ts'),
		},
		extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.scss']
	},
	module: {
		loaders: [
			{ test: /\.ts$/, loader: 'ts-loader' },
			{ test: /\.hbs/, loader: 'handlebars-template-loader' },
			{ 
				test: /\.scss$/i, 
				loader: ExtractTextPlugin.extract({
					fallback: "raw-loader",
					use: "raw-loader!sass-loader"
				}) 
			}
		]
	},
	plugins: get_plugins()
}