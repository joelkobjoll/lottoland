import {toRoman} from 'ProjectPath/library/helpers';

interface LottoResult {
    Winners: number;
    climbedSince: number;
    closingDate: string;
    currency: string;
    date: {
        day: number;
        dayOfWeek: string;
        full: string;
        hour: number;
        minute: number;
        month: number;
        year: number;
    }
    euroNumbers: number[];
    jackpot: number;
    lateClosingDate: string;
    marketingJackpot: number;
    nr: number;
    numbers: number[];
    odds: {
        [name: string]: {
            winners: number;
            specialPrize: number;
            prize: number;
        }        
    }
}

export interface LottoData {
    [name: string]: LottoResult;
}

export class Lotto {
    date: LottoResult['date'];
    odds: { [name: string]: string | number };
    currency: string;
    euroNumbers: number[];
    winningNumbers: number[];

    constructor(result: LottoResult) {
        this.date = result.date;
        this.currency = result.currency;
        this.euroNumbers = result.euroNumbers;
        this.winningNumbers = result.numbers;
        this.odds = Object.keys(result.odds).reduce((acc: any, key: string, index: number) => {
            let winner = result.odds[key];
            let rank = parseFloat(key.replace(/rank/, ''));

            if (winner.prize > 0) {
                acc.push({
                    rank: rank,
                    tier: toRoman(rank),
                    prize: new Intl.NumberFormat("en-EN", {
                        style: 'currency', 
                        currency: this.currency
                    }).format((winner.prize / 100)),
                    winners: new Intl.NumberFormat("en-EN").format(winner.winners)
                });
            }

            return acc;
        }, []).sort((a: any, b: any) => {
            return a.rank - b.rank;
        });
    }
}