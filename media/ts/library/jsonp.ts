export default class JSONP {
    private static clear(id: string) {
        const script: HTMLScriptElement = (<HTMLScriptElement>document.getElementById(id));

        document.head.removeChild(script);

        delete (window as any)[id];
    }

    static get(url: string) {
        let timeout: any;
        
        return new Promise((resolve, reject) => {
            const id = `JSON_${new Date().getTime()}`;
            const script = document.createElement('script');            

            script.id = id;
            script.src = `${url}?callback=${id}`;
            script.async = true;

            (window as any)[id] = function(json: any) {
                resolve(JSON.parse(JSON.stringify(json)));

                JSONP.clear(id);

                clearTimeout(timeout);
            };

            timeout = setTimeout(() => {
                reject(new Error(`JSONP request to ${url} timed out.`));

                JSONP.clear(id);
            }, 5000);

            document.head.appendChild(script);
        })
    }
}