import JSONP from 'ProjectPath/library/jsonp';
import {Lotto, LottoData} from 'ProjectPath/partials/lotto';

(async function() {
    try {
        const data = (await JSONP.get('https://www.lottoland.com/api/drawings/euroJackpot') as LottoData);

        if (data.last) {
            const eurojackpot = new Lotto(data.last);
            const template = require('ProjectPath/views/eurojackpot.hbs');
            const element = document.getElementById('eurojackpot');

            console.log(eurojackpot);

            if (element) {
                element.insertAdjacentHTML('afterbegin', template({
                    date: eurojackpot.date,
                    odds: eurojackpot.odds,
                    numbers: eurojackpot.winningNumbers,
                    euroNumbers: eurojackpot.euroNumbers
                }));                
            }
        }
    } catch(err) {
        console.warn(err);
    }
})();